# YOLOv3, pytorch implemtation for HPT

## Usage

Open a terminal in the folder of this project and run:

```
python3 detector.py -v --cuda -i input/{file}.mp4
```

Your output files will be ready in `detection/det_{file}.avi` and `detection/csv_{file}.csv`

Example output videos:

- https://www.youtube.com/watch?v=P9P1pZF2sZo
- https://www.youtube.com/watch?v=jJUQpLHlek8
- https://www.youtube.com/watch?v=Wn2Zz4pesLg

## Requirements

Tested with:

- Python 3.6.9
- Pytorch 1.4.0
- OpenCV 3.2.0
- Cuda 9.1 (for GPU calculations)

## Installation

1. Make sure all depencies (listed in requirements) are installed
2. Download https://pjreddie.com/media/files/yolov3.weights to project root

## Reference
- Fork from: https://github.com/zhaoyanglijoey/yolov3
- Paper [YOLOv3: An Incremental Improvement](https://pjreddie.com/media/files/papers/YOLOv3.pdf)
- Paper [Website](https://pjreddie.com/darknet/yolo/)
- [Tutorial](https://blog.paperspace.com/how-to-implement-a-yolo-object-detector-in-pytorch/)
